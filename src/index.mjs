// Copyright (c) 2022 Massimo Ghisalberti
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

// esversion: 6

import {
  app,
  BrowserView,
  BrowserWindow,
  dialog,
  Menu,
  MenuItem,
  netLog,
  screen,
  session,
  shell,
} from "electron";

import path from "path";
import { readFile, readFileSync } from "fs";
import console from "console";
import contextMenu from "electron-context-menu";

const MODE_IS_DEV = !app.isPackaged;
const APP_PATH = app.getAppPath();
const CONF_PATH = MODE_IS_DEV
  ? path.join(app.getAppPath(), "configurations")
  : path.dirname(APP_PATH);
let CONFIGURATION = {};
let Plugins = {};
let exports = { Main: {} };
let ctxMenuDispose;

const DEFAULT_USER_AGENT =
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36";

const BROWSER_OPTIONS = {
  useContentSize: true,
  webPreferences: {
    nodeIntegration: true,
    webSecurity: false,
    allowRunningInsecureContent: true,
    experimentalFeatures: true,
  },
};

const CONTEXT_MENU = {
  showLookUpSelection: true,
  showSelectAll: true,
  showCopyImage: true,
  showCopyImageAddress: true,
  showSaveImageAs: true,
  showCopyVideoAddress: true,
  showSaveVideo: true,
  showSaveVideoAs: true,
  showCopyLink: true,
  showSaveLinkAs: true,
  showInspectElement: true,
  showServices: true,
  showSearchWithGoogle: false,
};

export const messageBox = (title, message, window) =>
  dialog.showMessageBox(window, {
    title,
    message,
  });

const setBrowserBounds = (window, browser) => {
  const {
    width,
    height,
  } = window.getContentBounds();
  browser.setBounds({
    x: 0,
    y: 0,
    width,
    height,
  });
};

const adjustBrowserViewResize = (window) => {
  let lastHandle;
  const handleWindowResize = (e) => {
    e.preventDefault();
    lastHandle = setTimeout(() => {
      if (lastHandle != null) clearTimeout(lastHandle);
      const browser = window.getBrowserView();
      setBrowserBounds(window, browser);
    });
  };
  window.on("resize", handleWindowResize);
};

const getItem = (itemKey) => CONFIGURATION.sites[itemKey];

// async/await ???
const openItem = (window, itemKey, bounds) => {
  const item = getItem(itemKey);
  if (!item.loaded) {
    const userAgent = item.userAgent !== null
      ? item.userAgent
      : DEFAULT_USER_AGENT;
    item.ref.webContents.loadURL(item.link, {
      userAgent,
    });
    item.loaded = !item.reload;
  }
  window.setBrowserView(item.ref);
  setBrowserBounds(window, item.ref);
};

const prepareCssInjection = (site) => {
  let source = "";
  if (site.css && (site.css.enable === undefined ? true : site.css.enable)) {
    switch (site.css.type) {
      case "source":
        source = site.css.source;
        break;
      default:
        source = readFileSync(path.join(CONF_PATH, site.css.source), "utf-8");
    }
    source = `
    const __injectCss = (() => {    
      const customStyleTag = document.createElement("style");
      customStyleTag.textContent = \`${source}\`;
      document.head.appendChild(customStyleTag); 
    })();
  `;
  }
  return source;
};

const prepareJsInjection = (site) => {
  let source = "";
  if (
    site.script &&
    (site.script.enable === undefined ? true : site.script.enable)
  ) {
    switch (site.script.type) {
      case "source":
        source = site.script.source;
        break;
      default:
        source = readFileSync(
          path.join(CONF_PATH, site.script.source),
          "utf-8",
        );
    }
  }
  return source;
};

const prepareInjection = (site) => {
  const cssExe = prepareCssInjection(site);
  const jsExe = prepareJsInjection(site);
  site.ref.webContents.on("did-finish-load", () => {
    if (jsExe !== "") {
      site.ref.webContents.executeJavaScript(jsExe);
    }
    if (cssExe !== "") {
      site.ref.webContents.executeJavaScript(cssExe);
    }
  });
};

const menuOpenSiteHandler = (k) => {
  const site = CONFIGURATION.sites[k];
  return ((menuItem, win) => {
    openItem(win, k, win.getBounds());
    win.setTitle(site.title);
  });
};

const menuOpenDevToolsHandler = (k) => {
  const site = CONFIGURATION.sites[k];
  return ((menuItem, win) => {
    site.ref.webContents.openDevTools({
      mode: "detach",
    });
  });
};

const prepareExtMenu = (menuStruct, parentMenu, separator = false) => {
  if (menuStruct && menuStruct.items) {
    const items = menuStruct.items;
    prepareExternalMenuJs(menuStruct);
    if (separator) {
      parentMenu.append(new MenuItem({ type: "separator" }));
    }
    items.forEach((m) => {
      const i = new MenuItem({
        label: m.label,
        click: () => {
          Plugins[constantize(menuStruct.source)][m.handler](exports.Main);
        },
      });
      parentMenu.append(i);
    });
  }
};

const prepareSiteMenu = (win, k) => {
  const site = CONFIGURATION.sites[k];
  const browser = new BrowserView(BROWSER_OPTIONS);
  const subMenu = new Menu();
  CONTEXT_MENU.window = browser.webContents;
  CONTEXT_MENU.prepend = (defaultActions, parameters, browserWindow) => [
    {
      label: `${CONFIGURATION.ctxSearchEngine.label} "{selection}"`,
      visible: parameters.selectionText.trim().length > 0,
      click: () => {
        shell.openExternal(
          `${CONFIGURATION.ctxSearchEngine.url}?${CONFIGURATION.ctxSearchEngine.q}=${
            encodeURIComponent(parameters.selectionText)
          }`,
        );
      },
    },
  ];

  ctxMenuDispose = contextMenu(CONTEXT_MENU);

  browser.setAutoResize({
    width: true,
    height: true,
  });

  win.browsers.push(browser);
  site.ref = browser;
  site.loaded = false;

  prepareInjection(site);

  const menu = new MenuItem({
    submenu: subMenu,
    label: site.title,
  });

  const menuItemOpen = new MenuItem({
    site,
    label: "open...",
    click: menuOpenSiteHandler(k),
  });
  subMenu.append(menuItemOpen);

  if (site.tools) {
    const menuItemDev = new MenuItem({
      label: "dev tools",
      click: menuOpenDevToolsHandler(k),
    });
    subMenu.append(menuItemDev);
  }
  prepareExtMenu(site.menu, subMenu, true);
  return menu;
};

const prepareMainMenu = (win) => {
  const mainMenu = new Menu();
  const sitesKey = Object.keys(CONFIGURATION.sites);
  win.browsers = [];

  sitesKey.forEach((k) => {
    mainMenu.append(prepareSiteMenu(win, k));
  });

  prepareExtMenu(CONFIGURATION.menu, mainMenu);

  Menu.setApplicationMenu(mainMenu);

  if (CONFIGURATION.defaultSite) {
    openItem(win, CONFIGURATION.defaultSite, win.getBounds());
  }
};

const capitalize = (s) => s.replace(/^\w/, (c) => c.toUpperCase());
const constantize = (fname) => {
  return path.basename(fname, ".js").match(/\w*[^\W]/gm).map((e) =>
    capitalize(e)
  ).join("");
};

const prepareExports = (mainWindow) => {
  exports.Main = {
    App: app,
    Window: mainWindow,
    Configuration: CONFIGURATION,
    AppPath: APP_PATH,
    ConfPath: CONF_PATH,
    WebRequest: session.defaultSession.webRequest,
    messageBox: (title, message) => messageBox(title, message, mainWindow),
  };
};

const prepareExternalMainJs = (mainWindow) => {
  if (CONFIGURATION.source) {
    const plug = constantize(CONFIGURATION.source);
    import(path.join(CONF_PATH, CONFIGURATION.source)).then((data) =>
      Plugins[plug] = data
    );
  }
};

const prepareExternalMenuJs = (menu) => {
  if (menu.source) {
    const plug = constantize(menu.source);
    import(path.join(CONF_PATH, menu.source)).then((data) =>
      Plugins[plug] = data
    );
  }
};

const createWindow = () => {
  const options = {
    ...BROWSER_OPTIONS,
  };
  options.icon = path.join(APP_PATH, "assets/icon.png");
  const mainWindow = new BrowserWindow(options);
  readFile(path.join(CONF_PATH, "conf.json"), "utf-8", (err, data) => {
    if (err) {
      messageBox("Error", "Error reading conf :" + err.message, mainWindow);
      return;
    }
    CONFIGURATION = JSON.parse(data);
    prepareExports(mainWindow);
    prepareExternalMainJs(mainWindow);
    prepareMainMenu(mainWindow);
    adjustBrowserViewResize(mainWindow);
  });
};

const contentSecurityPolicyFix = (details, callback) => {
  //session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
  //console.log(details);
  const rhd = details.responseHeaders;
  if (
    rhd["Content-Security-Policy"] ||
    rhd["content-security-policy"]
  ) {
    const [cspName, csp] = rhd["content-security-policy"]
      ? [
        "content-security-policy",
        rhd["content-security-policy"],
      ]
      : [
        "Content-Security-Policy",
        rhd["Content-Security-Policy"],
      ];

    const policies = csp[0]
      .split(";").map((v) => {
        let policy = v.trim().split(" ");
        const polName = policy[0];
        const isScriptOrStyle = polName === "script-src" ||
          polName === "style-src";
        if (isScriptOrStyle) {
          policy = policy.filter((v) =>
            !v.match("nonce-") && !(v.match("sha256-") ||
              v.match("sha384-") ||
              v.match("sha512-"))
          );
        }
        return isScriptOrStyle
          ? `${policy.join(" ")} * 'unsafe-inline'`
          : policy.join(" ");
      }, {});
    const headers = {
      responseHeaders: {
        ...rhd,
        [cspName]: [policies.join(" ; ")],
      },
    };
    callback(headers);
  } else {
    callback(rhd);
  }
  //});
};

const handleWebRequestRequests = () => {
  const wb = session.defaultSession.webRequest;

  wb.onHeadersReceived((details, callback) => {
    if (CONFIGURATION.logWebRequestRequests) {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onHeadersReceived");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
    }
    contentSecurityPolicyFix(details, callback);
  });

  if (CONFIGURATION.logWebRequestRequests) {
    wb.onBeforeRequest((details, callback) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onBeforeRequest");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      callback(details);
    });

    wb.onBeforeSendHeaders((details, callback) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onBeforeSendHeaders");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      callback(details);
    });

    wb.onSendHeaders((details) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onSendHeaders");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
    });

    wb.onResponseStarted((details) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onResponseStarted");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
    });

    wb.onBeforeRedirect((details) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onBeforeRedirect");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
    });

    wb.onCompleted((details) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onCompleted");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
    });

    wb.onErrorOccurred((details) => {
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log("onErrorOccurred");
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
      console.log(details);
      console.log(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
      );
    });
  }
};

const onReadyApp = () => {
  const {
    width,
    height,
  } = screen.getPrimaryDisplay().workAreaSize;
  (BROWSER_OPTIONS.width = width / 1.5),
    (BROWSER_OPTIONS.height = height / 1.2),
    createWindow();
  handleWebRequestRequests();
};

app.on("ready", onReadyApp);

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    onReadyApp();
  }
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

/*
app.whenReady().then(async () => {
  await netLog.startLogging('./net-log.txt')
  // After some network events
  const path = await netLog.stopLogging()
  console.log('Net-logs written to', path)
})
*/

export const Main = exports.Main;
