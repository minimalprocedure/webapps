const fs = require("fs");
const path = require("path");

//import fs from "fs";
//import path from "path";

module.exports = {
  packagerConfig: {
    icon: "./assets/icon",
    asar: true,
  },
  ignore: [
    "configurations",
    ""
  ],
  hooks: {
    postPackage: async (forgeConfig, options) => {

      fs.copyFileSync(
        "./configurations/conf.json",
        path.join(options.outputPaths[0], "resources", "conf.json")
      );

      fs.copyFileSync(
        "./assets/icon.png",
        path.join(options.outputPaths[0], "icon.png")
      );

      if (options.spinner) {
        options.spinner.info(
          `Completed packaging for ${options.platform} / ${options.arch} at ${options.outputPaths[0]}`
        );
      }
    },
  },
  makers: [
    {
      name: "@electron-forge/maker-squirrel",
      config: {
        name: "Webapps",
        icon: "./assets/icon.ico",
      },
    },
    {
      name: "@electron-forge/maker-zip",
      platforms: ["linux", "win32"],
    },
    {
      name: "@electron-forge/maker-deb",
      config: {
        icon: "./assets/icon.png",
      },
    }
  ],
};
